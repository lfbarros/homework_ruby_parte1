require 'logger'

@logger = Logger.new("calc.log")

def ad(param1,param2)
    calc = Float(param1) + Float(param2)
    @logger.info "O resultado de #{param1} + #{param2} é igual a #{calc}"
end

def sub(param1,param2)
    calc = Float(param1) - Float(param2)
    @logger.info "O resultado de #{param1} - #{param2} é igual a #{calc}"
end

def div(param1,param2)
    calc = Float(param1) / Float(param2)
    @logger.info "O resultado de #{param1} / #{param2} é igual a #{calc}"
end

def multi(param1,param2)
    calc = Float(param1) * Float(param2)
    @logger.info "O resultado de #{param1} * #{param2} é igual a #{calc}"
end
